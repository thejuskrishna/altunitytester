.. AltUnity Tester documentation master file, created by
   sphinx-quickstart on Thu Oct 17 09:19:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AltUnity Tester's documentation!
======================================

.. toctree::
   :maxdepth: 3
   
   pages/overview
   pages/commands
   pages/building-games
   pages/download-import
   pages/writting-running-tests
   pages/examples
   pages/contributing
   pages/other/by
   pages/other/java-builders


   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
